// Değişkenler.
// var, let ve const ile tanımlanabilir.

var isim = "Sakız";
let yas = 2;
var geldiMi = false;
console.log(isim);
console.log(yas);


/** var yeniIsim = "Orhan";
console.log(yeniIsim + " buraya geliyor");
var isimler = "Sakız, Gusta";
isimler += "deneme";
console.log(yas+=1);
var diziIsimler = ["Sakız", "Gusta"];
diziIsimler.push("Deneme");
console.log(isimler[1]);
console.log(diziIsimler[0]);
console.log('isimler',isimler);
console.log('diziIsimler',diziIsimler);
console.log(typeof isim, typeof yas);
if (typeof isim == "string") {
    console.log(true);
    geldiMi = true;
    console.log(geldiMi);    
} else {
    console.log(false);
} */
//                    0        1       2
var markalar = ["Mercedes","BMW", "Volvo"];
for (var i = 0; i < markalar.length; i++) {
    var element = markalar[i];
    console.log(element + " güzel araba.");
}

/**for(var i = markalar.length-1; i >= 0; i--) {
    var element = markalar[i];
    console.log(markalar[i] + " güzel araba.");
}*/

/**markalar.forEach(element => {
    console.log(element + " güzel araba.")
});

for (const marka of markalar) {
    console.log(marka)
}**/

/*console.log(markalar[0]);
console.log(markalar[1]);
console.log(markalar[2]);*/

/** var sayiIlk = "100";
var sayiIki = "5";
console.log(parseInt(sayiIlk) + parseInt(sayiIki));
console.log(Number(sayiIki) + Number(sayiIlk)); */

// const kitap = 'Sakız kitap';
// kitap = 'Gusta kitap'
// console.log(kitap)
// let kitapSayisi = 0;


// function okunanKitapSayisi() {
//     kitapSayisi = 3;
//     console.log('okunan-kitap-sayısı',kitapSayisi)
// }

// console.log('kitap sayısı',kitapSayisi);
// okunanKitapSayisi();
// let yeniMarkalar = ["mercedes","volvo"]
// for (let index = 0; index < yeniMarkalar.length; index++) {
//     const element = yeniMarkalar[index];
//     console.log('element',element)
// }
// console.log('index',index)

// Array - dizi methodları
let evcilHayvanlar = ["Gusta"];
// push - Diziye yeni eleman ekler.
evcilHayvanlar.push("Sakız");
// map - döngü
evcilHayvanlar.map(e =>{
    console.log(e)
});
// pop - Dizi eleman çıkarma.
// evcilHayvanlar.pop();
// includes - Dizi elemanı içeriyor mu ? True yada false döner.
console.log(evcilHayvanlar.includes("Sakız"));
// indexOf - Dizi elemanının indexini getirir.
console.log(evcilHayvanlar.indexOf("Gustalar"));
// Dizinin uzunluğu
console.log(evcilHayvanlar.length)
// function isimGetir() {

// }
// Arrow function.
// isimGetir = () => {

// }

if (evcilHayvanlar.indexOf('Sakız') == -1) {
    console.log('Aradığınız cins kedi bulunamadı');
} else {
    console.log('Aradığınız cins kedi bulundu.');
}

